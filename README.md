<div>
<img width="100%" height="93" src="logo_gitlab_fullwidth.svg">
</div>

<div align="center">
ScreenPlay is an open source cross plattform app for displaying Wallpaper, Widgets and AppDrawer. It is written in modern C++17/Qt5/QML. Binaries with workshop support are available for Windows/Linux/MacOSX via <a href="https://store.steampowered.com/about/">Steam</a>. 
Join our community: <a href="https://screen-play.app/">Homepage</a>, <a href="https://forum.screen-play.app/">Forum</a>
<br> Visit our <a href="https://kelteseth.gitlab.io/ScreenPlayDocs/"> Developer Documentation</a> and our <a href="https://kelteseth.gitlab.io/ScreenPlayDocs/"> Getting Started Guide</a>!
<br>
<!--<h4><a href="https://steamcommunity.com/app/672870/">Download ScreenPlay!</a></h4>-->
<h4> <a href="https://forum.screen-play.app/topic/2/we-are-open-for-alpha-testing-via-steam"> >> Get a free Steam alpha key here! <<</a> </h4>
</div>
<br>



# Contributing

### Installation
1. Install python 3
2. Install mkdocs:
``` bash
pip install mkdocs
pip install mkdocs-material
```
3. Download ScreenPlayDocs
``` bash
# HTTPS
git clone  https://gitlab.com/kelteseth/ScreenPlayDocs
```

### Contributing
1. Run mkdocs locally
``` bash
 mkdocs serve
```
2. Make changes
3. Create a pull request
