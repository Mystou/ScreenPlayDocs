# Welcome to the official ScreenPlay Documentation
Here you can learn more about the different aspects of ScreenPlay:

- [Contribute - Code, Design & Translations](contribute/contribute)

### ScreenPlay Content
- [Learn more about projects](project/project)
- [Widget creation and custom C++](widgets/widgets)
- [Video, QML and HTML Wallpaper](wallpaper/wallpaper)
- [Develop own Apps via the ScreenPlaySDK](sdk/sdk)