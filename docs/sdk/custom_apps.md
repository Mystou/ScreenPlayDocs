Custom application who no not use Qt need to replicate the following features to work with ScreenPlay:

1. Implement a platform abstract [QLocalSocket ](http://doc.qt.io/qt-5/qlocalsocket.html) alternative
2. Connect to the socket name "ScreenPlay" with the appID (is transferred as first parameter to your app)
3. Exit the app if the "LocalSocket" is disconnected