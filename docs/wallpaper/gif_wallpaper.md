### How to create a gif wallpaper

1. Go to Create -> Empty HTML Wallpaper
![create_gif_1](create_gif_1.png "create_gif_1")
2. Set a name. The rest of the fields are optional and can be changed later.
![create_gif_2](create_gif_2.png "create_gif_2")
3. Right click -> Open containging folder
![create_gif_3](create_gif_3.png "create_gif_3")
4. Copy your gif into the folder
5. Open the index.html and replace the content with
``` html
<html>
<head>
<style type="text/css">
        body{
            background-image: url('myFancy.gif');
            background-size: cover; 
        }
</style>
</head>
<body>
</body>
</html>
```
6. Change the name of the give  background-image: url('myFancy.gif'); to your filename



#### Optional - Use the gif as preview in ScreenPlay when hovering over the wallpaper
7. Open your project.json and add an entry previewGIF with myFancy.gif as value!
``` json
{
    "file": "index.html",
    "license": "Open Source - MIT/Apache2",
    "preview": "dance.png",
    "previewThumbnail": "dance.png",
    "previewGIF": "myFancy.gif",
    "tags": [
        "Gif",
        "LowPoly"
    ],
    "title": "My Awsome Gif",
    "type": "htmlWallpaper"
}

```
8. Refresh the Installed list via F5 or "pull to refresh"
