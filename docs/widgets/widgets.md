Widgets are custom QML files which gets executed via the ScreenPlayWidget app. You can learn more about how to develop your widget in qml (even without prior programming skill!) via the fantastic qml book.

- [Getting started with qml](http://qmlbook.github.io/)

### Example Project

```javascript
    - FolderName
        -- previewGIF.gif            // Optional
        -- previewWEBM.webm          // Optional
        -- previewThumbnail.png      // Required
        -- preview.png               // Required
 
        -- project.json              // Required
        -- main.qml                  // Required
```

#### main.qml
```qml
import QtQuick 2.14
import QtQuick.Controls 2.14

// Custom size and color
property int widgetWidth: 300
property int widgetHeight: 200
property real widgetBackgroundOpacity: .2
property color widgetBackground: "red"
    

Item {
    id:root
    anchors.fill:parent

    Button {
        id:txtHello
        text: qsTr("Hello")
        anchors.centerIn:parent
        onClicked:{
            txtHello.text = "Hello Clicked"
        }
    }
}

```

#### project.json
```json
{
    "description": "This is a minimal hello world example ",
    "file": "main.qml",
    "preview": "preview.png",
    "title": "Hello World",
    "type": "qmlScene"
}

```

### Using Sysinfo to display hardware stats
Just import the SysInfo project in the beginning of your QML file

```qml
import ScreenPlay.Sysinfo 1.0

 // CPU
 - SysInfo.cpu.usage 
 - SysInfo.cpu.tickRate //How often do we update the value? Default every 1s

 // RAM
 - SysInfo.ram.usage

 - SysInfo.ram.usedPhysicalMemory
 - SysInfo.ram.totalPhysicalMemory

 - SysInfo.ram.usedVirtualMemory
 - SysInfo.ram.totalVirtualMemory

 - SysInfo.ram.usedPagingMemory
 - SysInfo.ram.totalPagingMemory

  // Storage (list model)
  // These variables are available for every listed storage device
  - name
  - displayName
  - isReadOnly
  - isReady
  - isRoot
  - isValid
  - bytesAvailable
  - bytesTotal
  - bytesFree
  - fileSystemType
```

##### Examples:
 - [Example CPU](example_CPU.md) 

![CPU Usage example](cpu_widget.png "CPU Usage example")

 - [Example Storage](example_Storage.md)

![Storage info example](storage_widget.png "Storage info example")

 - [Example Storage](example_RSS.md)
 
![RSS example](rss_widget.png "RSS  example")

### Sysinfo platform support

| Feature                	| Windows 	| Linux 	| MacOS 	|
|------------------------	|---------	|-------	|-------	|
| __CPU__              	    | ✔       	| ❌       | ❌     	|
| __RAM__                   | ✔       	| ❌       | ❌     	|
| __Storage__               | ✔        	|  ✔      |   ✔   	|
| __Network__               | ❌        	|  ❌     |  ❌     	|
| __GPU__       	        | ❌        	| ❌    	 |     ❌  	|


### Custom C++ Logic
You can add custom QML plugins in the root folder of your widget. These then get automatically loaded at startup.

 - [Creating C++ Plugins for QML](http://doc.qt.io/qt-5/qtqml-modules-cppplugins.html)
