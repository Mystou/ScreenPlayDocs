
![CPU Usage example](cpu_widget.png "CPU Usage example")


```qml
import QtQuick 2.14
import QtQuick.Controls 2.14
import ScreenPlay.Sysinfo 1.0

Item {
    id: root

    Text {
        id: type
        anchors {
            horizontalCenter: parent.horizontalCenter
            top: name.bottom
            topMargin: 50
        }

        text: qsTr("CPU USAGE")
        font.pointSize: 22
        color: "white"
        font.family: "Segoe UI"
    }
    Text {
        id: name
        anchors.centerIn: parent
        text: Math.floor(SysInfo.cpu.usage)
        font.pointSize: 32
        color: "white"
        font.family: "Segoe UI"
    }

    ProgressBar {
        from: 0
        to: 100
        value: Math.floor(SysInfo.cpu.usage)
        anchors {
            horizontalCenter: parent.horizontalCenter
            top: name.bottom
            topMargin: 10
        }
    }
}

```
