### How can I help contribute to ScreenPlay?

1. Install Qt (our c++ framework for developing ScreenPlay)
    1. You need an Qt account for the installer. [Register](https://login.qt.io/register)
    2. Download the [installer](https://www.qt.io/download-qt-installer)
    3. Start the installer and just leave everything to the default settings until your at this page:
![qt installer](qt_installer.png "qt_installer")
    4. Select the most recent Qt version (5.14.1 in this example) MSVC 2017 x64 and the Qt WebEngine
    5. Install

2. Fork ScreenPlay with you gitlab.com account
3. Download ScreenPlay via git.
   1. Install [git](https://git-scm.com/)
      * You can install git with the default settings!
   2. Install a git gui[GitExtentions](https://gitextensions.github.io/)
      * You need to setup your gitlab email and username after the installation!
      * As a merge tool is [vscode (free)](https://code.visualstudio.com/) the best
4. Select the blue Clone button from your forked ScreenPlay and select the "Clone with HTTPS" url
5. Download via  git extentions
![git extetnions](git_extetnions.png "git_extetnions")
6. Open The cloned folder and double click on the ScreenPlay.pro (pro as in project file. Like a Visual studio solution)
