### I encountered a bug. What can I do to help?
First check if other users encountered this bug. 
* Forum troubleshooting section: [forum.screen-play.app](https://forum.screen-play.app/category/7/troubleshooting)
* Gitlab issue tracker sorted by bugs: [gitlab.com/kelteseth/ScreenPlay](https://gitlab.com/kelteseth/ScreenPlay/issues?label_name%5B%5D=Bug)

### When you bug is not yet reported:

1. Create an [gitlab.com account](https://gitlab.com/users/sign_up)
2. Create a [new issue](https://gitlab.com/kelteseth/ScreenPlay/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)
     1. Choose the description template **Bug**
![new issue](new_issue.png "new issue")
     2. Fill out the sections:
         1. Summary - (Summarize the bug encountered concisely)
         2. Steps to reproduce - (How one can reproduce the issue - this is very important)
         3. Example Project - Like a video file, a wallpaper or a widget as a zip file!
         4. What is the current *bug* behavior?
         5. What is the expected *correct* behavior?
         6. Relevant logs and/or screenshots
         7. Possible fixes
     3. Select additional labels like Windows 7 (the bug label gets automatically added!)


### An certain issue is inportant to me. How can I show my support?
If you have a gitlab.com account you can vote on issues. This helps us greatly to determine on what to work next!
![new vote](issue_vote.png "new vote")
