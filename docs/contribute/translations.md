### How can I help with the translations/incorrect spelling?

1. Setup your ScreenPlay environment as described in the 5 minute [base setup guide](https://kelteseth.gitlab.io/ScreenPlayDocs/contribute/base_setup/)
2. Open the QLinguist  at C:\Qt\<yourQtVersionHere>\msvc2017_64\bin 
![linguist](linguist.png "linguist")
3. Open the translations file in the translation ScreenPlay git source folder
![ScreenPlay_translations](ScreenPlay_translations.png "ScreenPlay_translations")
4. Change them and save.


### How do I add new languages?
1. Copy a pre exsisting langauge file and change the name
2. Search for your langauge code (french in this example)
   * http://www.lingoes.net/en/translator/langcode.htm
3. Open your langauge ts ScreenPlay_fr.ts and change the language="??????" to your liking
4. Translate text by text. [DeepL](https://www.deepl.com/translator) can help with some languages!
5. Open the ScreenPlay.pro via QtCreator
6. Add your newly created file to the ScreenPlay.pro inside the ScreenPlay subfolder
![Add_to_project](Add_to_project.png "Add_to_project")
7. Also add it to the ScreenPlay Rsources.qrc file via QtCreator
![Add_resource](Add_resource.png "Add_resource")
8. Update translations
![update_tramslations](update_tramslations.png "update_tramslations")
9. Commit you changes
10. Create a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/)
